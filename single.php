<?php get_header(); ?>


<div class="wrapper detail">
    <?php if (have_posts()) : while (have_posts()) : the_post(); update_post_caches($posts); ?>
    <div class="page-header clear-filter" filter-color="orange">
        <div class="page-header-image" data-parallax="true" style="background-image: url('<?php bloginfo('template_url'); ?>/assets/vendor/now/img/header.jpg');"></div>
        <div class="container">
            <div class="content-center header-content">
                <span class="category"><?php the_category(' '); ?></span>
                <h1 class="title"><?php the_title(); ?></h1>
            </div>
            <div class="author-content">
                <img src="<?php echo esc_url( get_avatar_url( get_the_author_meta( 'user_email' ) ) ); ?>" alt="<?php echo get_the_author_meta('nickname') ?>" class="rounded-circle img-raised">
                <p class="meta">
                    <span><?php the_time(__('Y/m/d H:i', 'wp-candy')) ?></span>
                    <span><?php echo get_the_author_meta('nickname') ?></span>
                    <span><?php comments_popup_link(__('No comments', 'wp-candy'), __('1 comment', 'wp-candy'), __('% comments', 'wp-candy'), '', __('Comments off', 'wp-candy')); ?></span>
                    <span><?php if(function_exists('getPostViews')) { echo getPostViews(get_the_ID()); echo __(" views", 'wp-candy');} ?></p></span>
                </p>
            </div>
        </div>
    </div>
    <div class="main container">
        <div class="section section-basic">
            <div class="article" id="post-<?php the_ID(); ?>">
                <div class="body">
                    <?php get_template_part( 'content', get_post_format() ); ?>
                    <?php wp_link_pages(array('before' =>'<div class="wp-page-link"><span>'.__( 'Page', 'wp-candy' ).'</span>','after' => '</div>','link_before'=>'<span class="current">','link_after'=>'</span>'));?>
                    <?php setPostViews(get_the_ID()); ?>
                </div>
            </div>
        </div>
    </div>
     <?php endwhile; else : ?>
        <div class="page-header clear-filter" filter-color="orange">
             <div class="page-header-image" data-parallax="true" style="background-image: url('<?php bloginfo('template_url'); ?>/assets/vendor/now/img/header.jpg');"></div>
         </div>
         <div class="main container">
             <div class="section section-basic">
                 <div class="article" id="post-<?php the_ID(); ?>">
                     <div class="body">
                         <style>
                          .pagination {display:none;}
                         </style>
                         <?php _e('Sorry, no posts matched your criteria.', 'wp-candy'); ?>
                     </div>
                 </div>
             </div>
         </div>
     <?php endif; ?>

    <div class="container">
         <nav class="pagination clearfix">
           <div class="wp-pagenavi">
             <?php lt_next_post_link("%link") ?>
             <?php lt_previous_post_link('%link') ?>
           </div>
         </nav>
    </div>

    <div class="container">
        <?php comments_template(); ?>
    </div>


    </div>
</div>
<?php get_footer(); ?>
<?php get_header(); ?>

<div class="wrapper index">
    <div class="page-header clear-filter" filter-color="orange">
        <div class="page-header-image" data-parallax="true" style="background-image: url('<?php bloginfo('template_url'); ?>/assets/vendor/now/img/header.jpg');"></div>
        <div class="container">
            <div class="content-center brand">
                <!--<img class="n-logo" src="<?php bloginfo('template_url'); ?>/assets/images/logo.png" alt="">-->
                <h1 class="h1-title"><a href="<?php bloginfo('url'); ?>" title="<?php bloginfo('name'); ?>"><?php bloginfo('name'); ?></a></h1>
                <h3><?php bloginfo('description'); ?></h3>
            </div>
        </div>
    </div>
    <div class="main container page-404">
        <div class="section section-basic">
            <article class="article">
                <div class="body">
                    <p><?php _e('O, This is 404 Page.', 'wp-candy'); ?></p>
                    <p><img id="f404" src="<?php bloginfo('template_url'); ?>/assets/images/404.png" alt="404 <?php echo _('The page not found!', 'wp-candy'); ?>" /></p>
                </div>
            </article>
        </div>
    </div>
</div>





	<!--<article id="article">
		<div class="errorbox">
			<?php _e('O, This is 404 Page.', 'wp-candy'); ?>
		</div>
		<img id="f404" src="<?php bloginfo('template_url'); ?>/images/404.png" alt="404 <?php echo _('The page not found!', 'wp-candy'); ?>" />
	</article>-->
</section>



<?php get_footer(); ?>
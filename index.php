<?php get_header(); ?>

<div class="wrapper index">
    <div class="page-header clear-filter" filter-color="orange">
        <div class="page-header-image" data-parallax="true" style="background-image: url('<?php bloginfo('template_url'); ?>/assets/vendor/now/img/header.jpg');"></div>
        <div class="container">
            <div class="content-center brand">
                <!--<img class="n-logo" src="<?php bloginfo('template_url'); ?>/assets/images/logo.png" alt="">-->
                <h1 class="h1-title"><a href="<?php bloginfo('url'); ?>" title="<?php bloginfo('name'); ?>"><?php bloginfo('name'); ?></a></h1>
                <h3><?php bloginfo('description'); ?></h3>
            </div>
            <!--<div class="arrow-down"><i class="now-ui-icons arrows-1_minimal-down"></i></div>-->
        </div>
    </div>
    <div class="main container">
        <div class="section section-basic">
        <?php if (have_posts()) : while (have_posts()) : the_post(); update_post_caches($posts); ?>
            <article class="article"  id="post-<?php the_ID(); ?>">
                <div class="header">
                    <h4><span><a href="<?php the_permalink() ?>" title="<?php the_title(); ?>" rel="bookmark"><?php the_title(); ?></a></span></h4>
                    <div class="meta text-muted">
                        <span><a title="<?php the_time('Y/m/d H:i:s') ?>" href="/<?php the_time('Y/m/d') ?>"><?php the_time(__('Y/m/d H:i', 'wp-candy')) ?></a></span>
                        <span><?php comments_popup_link(__('No comments', 'wp-candy'), __('1 comment', 'wp-candy'), __('% comments', 'wp-candy'), '', __('Comments off', 'wp-candy')); ?></span>
                        <span><?php if(function_exists('getPostViews')) { echo getPostViews(get_the_ID()); echo __(" views", 'wp-candy');} ?></span>
                    </div>
                </div>
                <div class="body">
                    <?php the_content(__('Read more...', 'wp-candy')); ?>
                    <?php wp_link_pages(array('before' =>'<div class="wp-page-link"><span>'.__( 'Page', 'wp-candy' ).'</span>','after' => '</div>','link_before'=>'<span class="current">','link_after'=>'</span>'));?>
                </div>
            </article>
            <?php endwhile; else : ?>
            <article class="article">
                <style>
                .pagination {display:none;}
                </style>
                <?php _e('Sorry, no posts matched your criteria.', 'wp-candy'); ?>
            </article>
            <?php endif; ?>
        </div>

        <nav class="pagination clearfix" id="pagination">
                <?php if(function_exists('wp_pagenavi')) : ?>
                  <?php wp_pagenavi() ?>
                <?php else : ?>
                <div class="wp-pagenavi">
                  <!--<span class="newer pages">-->
                    <?php
                      if (get_previous_posts_link()) {
                        previous_posts_link(__('Newer', 'wp-candy'));
                      }else {
                        echo '<span>'.__('None','wp-candy').'</span>';
                      }
                    ?>
                  <!--</span>-->
                  <!--<span class="older pages">-->
                    <?php
                      if (get_next_posts_link()){
                        next_posts_link(__('Older', 'wp-candy'));
                      }else {
                        echo '<span>'.__('None','wp-candy').'</span>';
                      }
                    ?>
                  <!--</span>-->
                </div>
            <?php endif; ?>
        </nav>


    </div>
</div>

<?php get_footer(); ?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />
    <!--<link rel="apple-touch-icon" sizes="76x76" href="<?php bloginfo('template_url'); ?>/assets/images/logo.png">
    <link rel="icon" type="image/png" href="<?php bloginfo('template_url'); ?>/assets/images/c.png">-->
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title><?php bloginfo('name'); ?><?php wp_title(); ?></title>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
    <link href="<?php bloginfo('template_url'); ?>/style.css" rel="stylesheet" />
    <!-- Fonts and icons -->
    <!--<link href="https://fonts.proxy.ustclug.org/css?family=Montserrat:400,700,200" rel="stylesheet" />-->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet" />
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.8/css/solid.css" integrity="sha384-v2Tw72dyUXeU3y4aM2Y0tBJQkGfplr39mxZqlTBDUZAb9BGoC40+rdFCG0m10lXk" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.8/css/fontawesome.css" integrity="sha384-q3jl8XQu1OpdLgGFvNRnPdj5VIlCvgsDQTQB6owSOHWlAurxul7f+JpUOVdAiJ5P" crossorigin="anonymous">
    <!-- CSS Files -->
    <link href="<?php bloginfo('template_url'); ?>/assets/vendor/now/css/bootstrap.min.css" rel="stylesheet" />
    <link href="<?php bloginfo('template_url'); ?>/assets/vendor/now/css/now-ui-kit.css?v=1.1.0" rel="stylesheet" />
    <link rel='index' title="<?php bloginfo('name'); ?>" href="<?php bloginfo('url'); ?>" />
    <link rel="canonical" href="<?php bloginfo('url'); ?>" />
    <link rel="alternate" type="application/rss+xml" title="<?php _e('RSS 2.0 - all posts', 'wp-candy'); ?>" href="<?php bloginfo('rss2_url'); ?>" />
    <link rel="alternate" type="application/rss+xml" title="<?php _e('RSS 2.0 - all comments', 'wp-candy'); ?>" href="<?php bloginfo('comments_rss2_url'); ?>" />
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
    <?php wp_head(); ?>
    <?php flush(); ?>
    <link href="<?php bloginfo('template_url'); ?>/assets/styles/style.css" rel="stylesheet" />
    <!-- CodeMirror CSS Files -->
    <!--<link href="<?php bloginfo('template_url'); ?>/assets/vendor/codemirror/codemirror.css" rel="stylesheet" />
    <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/assets/vendor/codemirror/theme/seti.css">-->
    <!-- CodeMirror CSS Files -->
    <link href="<?php bloginfo('template_url'); ?>/assets/vendor/prism/prism.css" rel="stylesheet" />
</head>

<body class="wp-page sidebar-collapse">
    <!-- Navbar -->
    <nav class="navbar navbar-expand-lg bg-primary fixed-top navbar-transparent " color-on-scroll="400">
        <div class="container">
        <div class="dropdown button-dropdown menu-list">
            <a href="#pablo" class="dropdown-toggle" id="navbarDropdown" data-toggle="dropdown">
                <span class="button-bar"></span>
                <span class="button-bar"></span>
                <span class="button-bar"></span>
            </a>
            <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                <!--<a class="dropdown-header">Dropdown header</a>
                <a class="dropdown-item" href="#">Action</a>
                <a class="dropdown-item" href="#">Another action</a>
                <a class="dropdown-item" href="#">Something else here</a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="#">Separated link</a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="#">One more separated link</a>-->

                <?php
                /*$categories = get_categories( array(
                    'orderby' => 'name',
                    'order'   => 'ASC',
                    'hide_empty' => true,
                ) );

                var_dump($categories);

                foreach( $categories as $category ) {
                    $category_link = sprintf(
                        '<a class="dropdown-item" href="%1$s" alt="%2$s">%3$s</a>',
                        esc_url( get_category_link( $category->term_id ) ),
                        esc_attr( sprintf( __( 'View all posts in %s', 'textdomain' ), $category->name ) ),
                        esc_html( $category->name )
                    );

                    //echo sprintf( esc_html__( '%s', 'textdomain' ), $category_link );
                    //echo '<p>' . sprintf( esc_html__( 'Description: %s', 'textdomain' ), $category->description ) . '</p>';
                    //echo '<p>' . sprintf( esc_html__( 'Post Count: %s', 'textdomain' ), $category->count ) . '</p>';
                }*/
                ?>

                <?php
                  wp_list_categories('title_li=0&orderby=name&show_count=0&depth=1&class=dropdown-item');//分类列表

                  $count_pages = wp_count_posts( 'page' ); //页面
                  if ( $count_pages->publish > 0) {
                  echo '<li class="dropdown-divider"></li>';
                    wp_list_pages('title_li=&depth=1');
                  }

                  if (function_exists( 'has_nav_menu' ) && has_nav_menu( 'primary' )) { //自定义菜单
                    echo '<li class="dropdown-divider"></li>';
                    wp_nav_menu(array( 'theme_location' => 'primary','container' => '','items_wrap' => '%3$s' ));
                  }
                ?>
            </ul>
        </div>
            <div class="navbar-translate">
                <a class="navbar-brand" href="/" rel="tooltip" title="<?php bloginfo('name'); ?>" data-placement="bottom">
                    <?php bloginfo('name'); ?>
                </a>
                <button class="navbar-toggler navbar-toggler" type="button" data-toggle="collapse" data-target="#navigation" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-bar bar1"></span>
                    <span class="navbar-toggler-bar bar2"></span>
                    <span class="navbar-toggler-bar bar3"></span>
                </button>
            </div>
            <div class="collapse navbar-collapse justify-content-end" id="navigation" data-nav-image="<?php bloginfo('template_url'); ?>/assets/images/blurred-image.jpg">
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link" href="https://komplete.io"><!-- 选中状态的 class btn btn-neutral-->
                            <i class="now-ui-icons gestures_tap-01"></i>
                            <p>Komplete</p>
                        </a>
                    </li>
                    <!--<li class="nav-item">
                        <a class="nav-link" href="https://kalendar.komplete.io" target="_blank">
                            <i class="now-ui-icons gestures_tap-01"></i>
                            <p>Kalendar</p>
                        </a>
                    </li>-->
                    <li class="nav-item">
                        <a class="nav-link" href="https://GraceApi.com" target="_blank">
                            <i class="now-ui-icons gestures_tap-01"></i>
                            <p>GraceApi</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="https://candy.komplete.io" target="_blank">
                            <i class="now-ui-icons gestures_tap-01"></i>
                            <p>Candy</p>
                        </a>
                    </li>
                    <!--<li class="nav-item">
                        <a class="nav-link" rel="tooltip" title="Follow us on Twitter" data-placement="bottom" href="https://twitter.com/CreativeTim" target="_blank">
                            <i class="fa fa-twitter"></i>
                            <p class="d-lg-none d-xl-none">Twitter</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" rel="tooltip" title="Like us on Facebook" data-placement="bottom" href="https://www.facebook.com/CreativeTim" target="_blank">
                            <i class="fa fa-facebook-square"></i>
                            <p class="d-lg-none d-xl-none">Facebook</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" rel="tooltip" title="Follow us on Instagram" data-placement="bottom" href="https://www.instagram.com/CreativeTimOfficial" target="_blank">
                            <i class="fa fa-instagram"></i>
                            <p class="d-lg-none d-xl-none">Instagram</p>
                        </a>
                    </li>-->
                </ul>
            </div>
        </div>
    </nav>
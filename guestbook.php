<?php
/*
Template Name:guestbook
*/
?>

<?php get_header(); ?>


<div class="wrapper index">
    <div class="page-header clear-filter" filter-color="orange">
        <div class="page-header-image" data-parallax="true" style="background-image: url('<?php bloginfo('template_url'); ?>/assets/vendor/now/img/header.jpg');"></div>
        <div class="container">
            <div class="content-center brand">
                <!--<img class="n-logo" src="<?php bloginfo('template_url'); ?>/assets/images/logo.png" alt="">-->
                <h1 class="h1-title"><a href="<?php bloginfo('url'); ?>" title="<?php bloginfo('name'); ?>"><?php bloginfo('name'); ?></a></h1>
                <h3><?php bloginfo('description'); ?></h3>
            </div>
        </div>
    </div>
    <div class="main container page-guest-book">
        <div class="section section-basic">
            <article class="article">
                <div class="body">
                <?php if (have_posts()) : while (have_posts()) : the_post(); update_post_caches($posts); ?>
                   <?php //the_content(); ?>
                    <?php endwhile; else : ?>
                    <p></p>
                    <?php endif; ?>
                </div>
            </article>
        </div>
    </div>

    <div class="container">
        <?php comments_template('/guest-comments.php'); ?>
    </div>

</div>



<?php get_footer(); ?>
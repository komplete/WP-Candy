<div class="wrapper">
    <!--  End Modal -->
    <footer class="footer" data-background-color="black">
        <div class="container">
            <!--<nav>
                <ul>
                    <li>
                        <a href="https://www.creative-tim.com">
                            Creative Tim
                        </a>
                    </li>
                    <li>
                        <a href="http://presentation.creative-tim.com">
                            About Us
                        </a>
                    </li>
                    <li>
                        <a href="http://blog.creative-tim.com">
                            Blog
                        </a>
                    </li>
                    <li>
                        <a href="https://github.com/creativetimofficial/now-ui-kit/blob/master/LICENSE.md">
                            MIT License
                        </a>
                    </li>
                </ul>
            </nav>-->
            <div class="copyright">
                <a href="http://www.miitbeian.gov.cn/" target="_blank">陕ICP备17016223号</a> &copy; <a title="<?php bloginfo('name'); ?>" href="<?php bloginfo('url'); ?>"><?php bloginfo('name'); ?></a> <?php echo __('Theme <a href="https://candy.komplete.io/">WP Candy</a> by <a href="https://yimity.com/" target="_blank">一米</a>','wp-candy'); ?> <?php echo __('Proudly powered by','wp-candy'); ?><a title="<?php echo __('Proudly powered by WordPress','wp-candy'); ?>" href="http://WordPress.org" target="_blank">WordPress</a>
            </div>
        </div>
    </footer>
</div>

</body>

<!--   Core JS Files   -->
<script src="<?php bloginfo('template_url'); ?>/assets/vendor/now/js/core/jquery.3.2.1.min.js" type="text/javascript"></script>
<script src="<?php bloginfo('template_url'); ?>/assets/vendor/now/js/core/popper.min.js" type="text/javascript"></script>
<script src="<?php bloginfo('template_url'); ?>/assets/vendor/now/js/core/bootstrap.min.js" type="text/javascript"></script>
<!--  Plugin for Switches, full documentation here: http://www.jque.re/plugins/version3/bootstrap.switch/ -->
<script src="<?php bloginfo('template_url'); ?>/assets/vendor/now/js/plugins/bootstrap-switch.js"></script>
<!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
<script src="<?php bloginfo('template_url'); ?>/assets/vendor/now/js/plugins/nouislider.min.js" type="text/javascript"></script>
<!--  Plugin for the DatePicker, full documentation here: https://github.com/uxsolutions/bootstrap-datepicker -->
<script src="<?php bloginfo('template_url'); ?>/assets/vendor/now/js/plugins/bootstrap-datepicker.js" type="text/javascript"></script>
<!-- Control Center for Now Ui Kit: parallax effects, scripts for the example pages etc -->
<script src="<?php bloginfo('template_url'); ?>/assets/vendor/now/js/now-ui-kit.js?v=1.1.0" type="text/javascript"></script>

<!--<script src="<?php bloginfo('template_url'); ?>/assets/vendor/codemirror/codemirror.js" type="text/javascript"></script>
<script src="<?php bloginfo('template_url'); ?>/assets/vendor/codemirror/mode/javascript/javascript.js"></script>
<script src="<?php bloginfo('template_url'); ?>/assets/vendor/codemirror/mode/xml/xml.js"></script>
<script src="<?php bloginfo('template_url'); ?>/assets/vendor/codemirror/mode/css/css.js"></script>
<script src="<?php bloginfo('template_url'); ?>/assets/vendor/codemirror/mode/htmlmixed/htmlmixed.js"></script>
<script src="<?php bloginfo('template_url'); ?>/assets/vendor/codemirror/addon/selection/active-line.js"></script>
<script src="<?php bloginfo('template_url'); ?>/assets/vendor/codemirror/addon/edit/matchbrackets.js"></script>-->


<script src="<?php bloginfo('template_url'); ?>/assets/vendor/prism/prism.js"></script>



<script src="<?php bloginfo('template_url'); ?>/assets/scripts/common.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/assets/scripts/comments.js"></script>


</html>
      <div class="title">
        <h4><a href="<?php the_permalink() ?>" title="<?php the_title(); ?>" rel="bookmark"><?php the_title(); ?></a></h4>
        <!--<span class="info"><?php comments_popup_link(__('No comments', 'wp-candy'), __('1 comment', 'wp-candy'), __('% comments', 'wp-candy'), '', __('Comments off', 'wp-candy')); ?>  /  <?php if(function_exists('getPostViews')) { echo getPostViews(get_the_ID()); echo __(" views", 'wp-candy');} ?></span>-->
      </div>
        <?php the_content(__('Read more...', 'wp-candy')); ?>

<?php
/*
Template Name:link
*/
?>

<?php get_header(); ?>


<div class="wrapper index">
    <div class="page-header clear-filter" filter-color="orange">
        <div class="page-header-image" data-parallax="true" style="background-image: url('<?php bloginfo('template_url'); ?>/assets/vendor/now/img/header.jpg');"></div>
        <div class="container">
            <div class="content-center brand">
                <!--<img class="n-logo" src="<?php bloginfo('template_url'); ?>/assets/images/logo.png" alt="">-->
                <h1 class="h1-title"><a href="<?php bloginfo('url'); ?>" title="<?php bloginfo('name'); ?>"><?php bloginfo('name'); ?></a></h1>
                <h3><?php bloginfo('description'); ?></h3>
            </div>
        </div>
    </div>
    <div class="main container page-links">
        <div class="section section-basic">
        <?php if (have_posts()) : while (have_posts()) : the_post(); update_post_caches($posts); ?>
            <article class="article" id="post-<?php the_ID(); ?>">
                <!--<div class="header">
                    <h4><span><?php the_title(); ?></span></h4>
                </div>-->
                <div class="body">
                   <?php the_content(); ?>
                </div>
            </article>
            <?php endwhile; else : ?>
                <p></p>
            <?php endif; ?>
        </div>
    </div>

    <div class="container">
        <?php comments_template(); ?>
    </div>

</div>



<?php get_footer(); ?>